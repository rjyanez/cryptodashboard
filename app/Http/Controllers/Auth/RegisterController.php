<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

use App\User;
use App\VerifyUser;
use App\Mail\VerifyMail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            // 'username' => 'required|string|max:255|unique:users',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            // 'phone' => 'required|numeric|min:7',
            'valdocument_id' => 'required|numeric',
            'valcountry_id' => 'required|numeric',
            'identification' => 'required|numeric|min:7',
        ]);
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'valdocument_id' => $data['valdocument_id'],
            'valcountry_id' => $data['valcountry_id'],
            'identification' => $data['identification'],
        ]);
 
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);
 
        Mail::to($user->email)->send(new VerifyMail($user));
 
        return $user;
    }

    public function verify($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){

            $user = $verifyUser->user;

            $this->flashMessage('info',"Please complete the information below.");
            return view('auth.register.verify',['user'=>$user]);
        }

        $this->flashMessage('warning',"Sorry your email cannot be identified.");
        return redirect(route('login'));
    }


    public function registeruser(Request $requests, $user_id){

        if($requests->hasFile('document_image')){

            $user = User::find($user_id);
            $user->verified=1;
            $user->username=$requests->username;
            $user->phone=$requests->phone;

            $file = $requests->file('document_image');
            $imageName = $user_id.'.'.$file->extension();
            $user->document_image=$file->storeAs('document_image',$imageName);

            if($user->save()){
                $this->flashMessage('success',"Your e-mail is already verified. You can now login.");
                return redirect(route('login'));
            } else{
                $this->flashMessage('warning',"Sorry your information cannot be save.");
            }
        } else{

            $this->flashMessage('warning',"Sorry your information cannot be save.");
        }

    }
}
