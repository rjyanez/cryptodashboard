<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username','phone','valdocument_id' ,'valcountry_id','identification','document_image'
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
 
    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    public function typeDocument()
    {
        return $this->belongsTo('App\Values', 'valdocument_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Values', 'valcountry_id');
    }
 
}
