@extends('layouts.default')
@section('content')
<section>
	<div class="row pb-2">
		<div class="col">
			<p class="text-black-50"> Kromium</p>
			<h4 class="text-primary">$ 324987.00</h4>
		</div>
		<div class="col border-left">
			<p class="text-black-50"> Bitcoin</p>
			<h4 class="text-muted">$ 8184.99</h4>
		</div>
		<div class="col border-left">
			<p class="text-black-50"> Ethereum</p>
			<h4 class="text-muted">$ 464.39</h4>
		</div>
		<div class="col border-left">
			<p class="text-black-50"> Litecoin</p>
			<h4 class="text-muted">$ 83.78</h4>
		</div>
		<div class="col border-left">
			<p class="text-black-50"> Dash</p>
			<h4 class="text-muted">$ 239.00</h4>
		</div>
		<div class="col border-left">
			<p class="text-black-50"> Petro</p>
			<h4 class="text-muted">$ 60.00</h4>
		</div>
	</div>
</section>
<section class="p-4">
	<div class="row pb-4">
        <div class="col">
            <div class="tile-body ">
            	<h3 class="tile-title">Order History</h3>
            </div>
        </div>
        <div class="col">
        	<button type="button" class="btn btn-outline-success rounded-0 float-right"><i class="fa fa-plus"></i>Nueva Orden</button>
        </div>
    </div>
    <div class="row">
        <div class="col">
          	<div class="tile">
	            <table class="table table-hover" id="sampleTable">
		            <thead>
		              	<tr>
			              <th>Codigo</th>
			              <th>Monto</th>
			              <th >Transaccion</th>
			              <th>Fecha</th>
			              <th>Estatus</th>
			            </tr>
		            </thead>
		            <tbody>
		                <tr class="text-success">
		                  <td>22356</td>
		                  <td>0.0034</td>
		                  <td>XtsPwAStgz7E9dcoWTBLEaVSgxn6ZJSTMw</td>
		                  <td>2011/04/25</td>
		                  <td>Exitosa</td>
		                </tr>
		                <tr class="text-danger">
		                  <td>343456</td>
		                  <td>0.1234</td>
		                  <td>Xv2cbVnxm5MTHfitngCDnGxMR3jmZNeCH3</td>
		                  <td>2011/07/25</td>
		                  <td>Fallida</td>
		                </tr>
		                <tr class="text-success">
		                  <td>45799</td>
		                  <td>0.2445</td>
		                  <td>Xx9zL6LvfQGJdKX2KHEH7LNFFw1asbnMmU</td>
		                  <td>2009/01/12</td>
		                  <td>Exitosa</td>
		                </tr>
		                <tr class="text-muted">
		                  <td>83546</td>
		                  <td>1.34</td>
		                  <td>Xg72AXcnWp4J6hYigY9yUVeMbtYCWYUyrV</td>
		            	  <td>2012/03/29</td>
		            	  <td>Procesando</td>
		            	</tr>
		            	<tr class="text-success">
		            	  <td>39560970</td>
		                  <td>2.35</td>
		                  <td>XqGinTVQdCGxLk4ngCmdF8D9xaXP42u5qQ</td>
		                  <td>2008/11/28</td>
		                  <td>Exitosa</td>
		                </tr>
		            </tbody>
	            </table>
        	</div>
       	</div>
    </div>
</section>
@stop
