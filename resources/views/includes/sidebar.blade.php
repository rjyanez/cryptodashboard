<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
   <ul class="app-menu">
      <li>
         <a class="app-menu__item active" href="index.html">
            <i class="app-menu__icon fa fa-dashboard"></i>
            <span class="app-menu__label">Home</span>
         </a>
      </li>
      <li>
         <a class="app-menu__item active" href="index.html">
            <i class="app-menu__icon fa fa-server"></i>
            <span class="app-menu__label">Order History</span>
         </a>
      </li>
      <li>
         <a class="app-menu__item active" href="index.html">
            <i class="app-menu__icon fa fa-exchange "></i>
            <span class="app-menu__label">Transaction History</span>
         </a>
      </li>      
   </ul>
</aside>