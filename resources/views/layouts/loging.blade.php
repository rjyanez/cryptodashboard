<!doctype html>
<html lang="{{ app()->getLocale() }}">
   <head>
      @include('includes.head') 
   </head>
   <body>
      <section class="material-half-bg">
         <div class="cover">
         </div>
      </section>
      <section class="login-content">
         @if (Session::has('alert-menssage'))
            <div class="col-10 card text-white bg-{{Session::get('alert-type')}}">
               <div class="card-body">
                  {{ Session::get('alert-menssage') }}                  
               </div>
            </div>
         @endif            
         <div class="logo">
            <h1>KROMIUM</h1>
         </div>
         @yield('content')
      </section>
      @include('includes.footer')
   </body>
</html>