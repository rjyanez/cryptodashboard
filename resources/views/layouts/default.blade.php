<!doctype html>
<html lang="{{ app()->getLocale() }}">
   <head>
      @include('includes.head') 
   </head>
   <body class="app sidebar-mini rtl">
      @include('includes.header')
      @include('includes.sidebar')
      <main class="app-content">
         @if (Session::has('alert-menssage'))
            <div class="col-10 alert alert-{{Session::get('alert-type')}} alert-dismissible fade show">
               {{ Session::get('alert-menssage') }}
            </div>
         @endif
         @yield('content')
      </main>
      @include('includes.footer')
   </body>
</html>