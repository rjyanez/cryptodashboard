@extends('layouts.loging')

@section('content')
<div class="login-box">
    <div class="card mb-0">
        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            <div class="card-body">
                    <h4 class="card-title pb-4">{{ __('Login') }}</h4>
                    @csrf
                    <div class="form-group">
                        <label for="email" class="text-md-right">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif        
                    </div>
                    <div class="form-group">
                        <label for="password" class="text-md-right">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <label class="form-check-label" for="remember">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">
                            {{ __('Login') }}
                        </button>
                    </div>
                </div>
                <div class="card-footer ">
                    <div class="row">                        
                        <div class="col">
                            <a class="btn btn-link" href="{{ route('register') }}">
                                {{ __('Register!') }}
                            </a>
                        </div>
                        <div class="col">
                            <a class="btn btn-link " href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        </div>                    
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
