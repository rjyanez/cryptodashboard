@extends('layouts.loging')
@section('content')
<div class="login-box ">
   <div class="card border border-0">
      <div class="card-body">     
      
      <form method="POST" enctype="multipart/form-data" action="{{ route('registeruser',$user->id) }}" aria-label="{{ __('Verificacion') }}">
         @csrf         
         <div class="row">
               <div class="col">
                  <h4 class="card-title pb-4">{{ __('Verificacion') }}</h4>                  
               </div>
         </div>
         <div class="row">   
               <div class="col">              
                  <div class="form-group ">
                     <label for="phone" class="text-md-right">{{ __('Telefono') }}</label>
                     <input id="phone" type="numeric" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus>
                     @if ($errors->has('phone'))
                        <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                     @endif                            
                  </div>
               </div>
               <div class="col">                  
                  <div class="form-group ">
                     <label for="username" class="text-md-right">{{ $user->valdocument_id == 239 ? __('Nombre del Representante') : __('Nombre de Usuario') }}</label>
                     <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
                     @if ($errors->has('username'))
                        <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('username') }}</strong>
                        </span>
                     @endif                            
                  </div>
               </div> 
         </div>
         <div class="row">
            <div class="col">
               <label class="text-md-right" for="document_image">{{ $user->valdocument_id == 239 ? __('Seleccione Acta') : __('Seleccione Imagen') }}</label>
               <input required type="file" class="form-control-file border {{ $errors->has('document_image') ? ' is-invalid' : '' }}"  name='document_image' id="document_image">

               @if ($errors->has('document_image'))
                  <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('document_image') }}</strong>
                  </span>
               @endif 
            </div>
         </div>                 
         <br>         
         <div class="form-group ">
            <button type="submit" class="btn btn-primary btn-block">
            {{ __('Verificar') }}
            </button>          
         </div>
      </form>
      </div>
   </div>
</div>
@endsection