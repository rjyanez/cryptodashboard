@extends('layouts.loging')
@section('content')
<div class="login-box register-box">
   <div class="card mb-0">
      <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
         @csrf
         <div class="card-body">
            <div class="row">
               <div class="col">
                  <h4 class="card-title pb-4">{{ __('Registro') }}</h4>                  
               </div>
               <div class="col">
                  <div class="btn-group">
                    <a href="{{ route('register.natural') }}" class="btn btn-outline-primary">Persona Natural</a>
                     <a href="{{ route('register.juridic') }}" class="btn btn-primary">Persona Juridica</a>
                  </div>            
               </div>
            </div>
            <div class="row">
                     <div class="col">
                        <div class="form-group ">
                           <label for="name" class="text-md-right">{{ __('Razon Social') }}</label>
                           <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                           @if ($errors->has('name'))
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('name') }}</strong>
                           </span>
                           @endif                            
                        </div>                 
                        <div class="form-group">
                           <label for="email" class="text-md-right">{{ __('Correo Electronico') }}</label>
                           <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                           @if ($errors->has('email'))
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('email') }}</strong>
                           </span>
                           @endif
                        </div>
                        <div class="form-group">
                           <label for="password" class="text-md-right">{{ __('Contraseña') }}</label>
                           <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                           @if ($errors->has('password'))
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('password') }}</strong>
                           </span>
                           @endif                          
                        </div>
                     </div>
                     <div class="col">                  
                        <div class="form-group ">
                           <label for="identification" class="text-md-right">{{ __('Identificacion') }}</label>
                           <div class="input-group">
                              <select id="valdocument_id" class="form-control {{ $errors->has('valdocument_id') ? ' is-invalid' : '' }}" name="valdocument_id" value="{{ old('valdocument_id') }}" required autofocus>
                              @foreach ($typeDocuments as $id=>$document)
                                 <option value="{{ $id }}" {{ $id == 239 ? 'selected' : '' }}>{{$document}}</option>
                              @endforeach
                              </select>
                              <input id="identification" type="numeric" class="form-control{{ $errors->has('identification') ? ' is-invalid' : '' }}" name="identification" value="{{ old('identification') }}" required autofocus>
                           </div>
                           @if ($errors->has('valdocument_id'))
                           <span class="invalid-feedback" role="alert">                  
                              <strong>{{ $errors->first('valdocument_id') }}</strong>
                           </span>
                           @endif  
                           @if ($errors->has('identification'))
                           <span class="invalid-feedback" role="alert">                  
                              <strong>{{ $errors->first('identification') }}</strong>
                           </span>
                           @endif                            
                        </div>
                        <div class="form-group ">
                           <label for="valcountry_id" class="text-md-right">{{ __('Pais de Origen') }}</label>
                           <select id="valcountry_id" class="form-control {{ $errors->has('valcountry_id') ? ' is-invalid' : '' }}" name="valcountry_id" value="{{ old('valcountry_id') }}" required autofocus>
                           @foreach ($countries as $id=>$country)
                              <option value="{{ $id }}" {{ $id == 230 ? 'selected' : '' }} >{{$country}}</option>
                           @endforeach
                           </select>
                           @if ($errors->has('valcountry_id'))
                           <span class="invalid-feedback" role="alert">                  
                              <strong>{{ $errors->first('valcountry_id') }}</strong>
                           </span>
                           @endif                            
                        </div>
                        <div class="form-group">
                           <label for="password-confirm" class="text-md-right">{{ __('Confirmar Contraseña') }}</label>
                           <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                     </div> 
            </div>
            <br>         
            <div class="form-group ">
               <button type="submit" class="btn btn-primary btn-block">
               {{ __('Registro') }}
               </button>          
            </div>
         </div>
         <div class="card-footer ">
            <div class="row">
               <div class="col">
                  <a class="btn btn-link" href="{{ route('login') }}">
                  {{ __('Login!') }}
                  </a>
               </div>
               <div class="col">
                  <a class="btn btn-link " href="{{ route('password.request') }}">
                  {{ __('Forgot Your Password?') }}
                  </a>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>
@endsection