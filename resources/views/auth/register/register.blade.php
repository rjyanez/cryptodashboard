@extends('layouts.loging')
@section('content')
<div class="login-box register-box">
   <div class="card mb-0">
      <div class="card-body d-flex flex-column justify-content-center">
         <h4 class="card-title pb-5 center">{{ __('Registro') }}</h4>
         <br>
         <div class="pb-5 pt-5 mb-5 d-flex flex-row justify-content-around">
               <a href="{{ route('register.natural') }}" class="btn btn-primary btn-lg">Persona Natural</a>
               <a href="{{ route('register.juridic') }}" class="btn btn-primary btn-lg">Persona Juridica</a>
         </div>                  
      </div>
      <div class="card-footer ">
            <div class="row">
               <div class="col">
                  <a class="btn btn-link" href="{{ route('login') }}">
                  {{ __('Login!') }}
                  </a>
               </div>
               <div class="col">
                  <a class="btn btn-link " href="{{ route('password.request') }}">
                  {{ __('Forgot Your Password?') }}
                  </a>
               </div>
            </div>
      </div>
   </div>
</div>
@endsection