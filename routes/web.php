<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.home');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verify')->name('verifyuser');
Route::post('/user/register/{user_id}','Auth\RegisterController@registeruser')->name('registeruser');